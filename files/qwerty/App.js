/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {AppRegistry,Platform, StyleSheet, Text, View, Image} from 'react-native';


// Props
// class Greeting extends Component{
//     render() {
//         return (
//             <View style={{alignItems: 'center'}}>
//                 <Text>Hello {this.props.name}</Text>
//             </View>
//         );
//     }
// }

class Blink extends Component{

    constructor(props){
        super(props);
        this.state = { isShowingText: true};

    setInterval(()=>(
    this.setState(previousState => (
        {isShowingText: !previousState.isShowingText}
    ))
    ),1000);
    }

    render() {
        if(!this.state.isShowingText){
            return null;
        }
        return (
            <Text>{this.props.text}</Text>
        );
    }
}








export default class App extends Component {
  render() {


    //Props Image
    // let pic = {
    //   uri : 'http://2.bp.blogspot.com/-pATX0YgNSFs/VP-82AQKcuI/AAAAAAAALSU/Vet9e7Qsjjw/s1600/Cat-hd-wallpapers.jpg'
    // };

    return (
        <View style={{paddingTop:30}}>
            <Blink text={'I love to Blink'}/>
            <Blink text={'Yes blinking is so great'}/>
            <Blink text={'Why did they ever take this out of HTML'}/>
            <Blink text={'Look at me look at me look at me'}/>
        </View>


        //Props
        //{/*<View style = {{alignItems: 'center', top:50}}>*/}
          //{/*<Greeting name='Rexxar'/>*/}
            //{/*<Greeting name='Jaina'/>*/}
            //{/*<Greeting name='Valeera'/>*/}
        //{/*</View>*/}




        //Props Image
        //{/*<Image source={pic} style={{alignItems : 'center',width: 193, height: 110}}/>*/}


        // Hello World
        // {/*<View style={{flex:1, justifyContent: "center", alignItems: "center"}}>*/}
        //  {/*<Text>Hello World</Text> */}
        // {/*<Text style={styles.instructions}>{instructions}</Text>*/}
        // {/*</View>*/}

    );

  }
}

AppRegistry.registerComponent('qwerty', ()=> App);


// {/*const styles = StyleSheet.create({*/}
//   {/*container: {*/}
//     {/*flex: 1,*/}
//     {/*justifyContent: 'center',*/}
//     {/*alignItems: 'center',*/}
//     {/*backgroundColor: '#F5FCFF',*/}
//   {/*},*/}
//   {/*welcome: {*/}
//     {/*fontSize: 20,*/}
//     {/*textAlign: 'center',*/}
//     {/*margin: 10,*/}
//   {/*},*/}
//   {/*instructions: {*/}
//     {/*textAlign: 'center',*/}
//     {/*color: '#333333',*/}
//     {/*marginBottom: 5,*/}
//   {/*},*/}
// {/*});*/}


