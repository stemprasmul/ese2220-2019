/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.container}>
          <Text style={styles.welcome}>Welcome, Hubert, to React Native!</Text>
          <Text style={styles.instructions}>To get started, edit App.js</Text>
          <Text style={styles.instructions}>{instructions}</Text>
          <Button title={"Press me!"}
                  color={'#288036'}
                  onPress={ () => {
                    console.log("Button pressed!")
                  }} />

          <Text style={styles.buttonInstructions}> Press the button for more information </Text>
        </View>

        <View style={styles.boxForText}>
          <Text style={styles.boxedText}> This is white on red </Text>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  buttonInstructions: {
    color: 'grey',
    marginTop: 0,
    marginBottom: 8,
    fontSize: 14,
  },
  boxForText: {
    flex: 1,
    backgroundColor: 'red',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    // height: 100,
  },
  boxedText: {
    fontSize: 20,
    color: 'white',
    textAlign: 'center',
  },
});
